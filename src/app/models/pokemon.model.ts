



export interface ApiPokemon {
    name: string,
    url: string
}


export interface Pokemon {
    id: string,
    url: string,
    name: string
}