import { ApiPokemon } from "./pokemon.model";


export interface ApiResponse {
    next: string | null;
    previous: string | null;
    results: ApiPokemon[];
}