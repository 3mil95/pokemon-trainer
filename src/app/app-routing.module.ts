import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WithAuthGuard } from 'src/guards/with-auth.guard';
import { WithoutAuthGuard } from 'src/guards/without-auth.guard';
import { CataloguePageComponent } from './pages/catalogue-page/catalogue-page.component';
import { StartPageComponent } from './pages/start-page/start-page.component';
import { TrainerPageComponent } from './pages/trainer-page/trainer-page.component';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: StartPageComponent,
    canActivate: [WithoutAuthGuard]
  },
  {
    path: "trainer",
    component: TrainerPageComponent,
    canActivate: [WithAuthGuard]
  },
  {
    path: "catalogue/:page",
    component: CataloguePageComponent,
    canActivate: [WithAuthGuard]
  },
  { path: '**', 
    redirectTo: '/catalogue/1' 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
