import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiResponse } from '../models/api-response.model';
import { ApiPokemon, Pokemon } from '../models/pokemon.model';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _pokemons : Pokemon[][] = [];
  private _pageSize : number = 60;
  private _hasNext : boolean = false;
  private _hasPrevious : boolean = false;
  private _page : number = 1;

  constructor(private readonly http : HttpClient) { }

  // Fetches a pokemon catalog page from the api.
  public fetchPokemons(page: number): void {
    this._page = page;
    if (this._pokemons[this._page] !== undefined) {
      return;
    }

    const offset = (page-1) * this._pageSize;
    const limit = this._pageSize;

    this.http.get<ApiResponse>(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=${limit}`).subscribe((response : ApiResponse) => {
      this._hasNext = (response.next) ? true : false;
      this._hasPrevious = (response.previous) ? true : false;

      this._pokemons[this._page] = response.results.map(
        (pokemon : ApiPokemon) : Pokemon => {
          const urlSplit = pokemon.url.split("/");
          pokemon.name = pokemon.name[0].toUpperCase() + pokemon.name.substring(1);
          return { ...pokemon, id: urlSplit[urlSplit.length - 2]};
        }, (error : HttpErrorResponse) => {
          console.error(error);
        }
      );
    })
  }

  // Returns the fetched pokemon.
  public pokemons() : Pokemon[] {
    return this._pokemons[this._page];
  }

  // Returns is the current page has a previous page.
  public hasPrevious() : boolean{
    return this._hasPrevious;
  }

  // Returns is the current page has a next page.
  public hasNext() : boolean {
    return this._hasNext;
  }

  // Returns the current page.
  public getPage() : number {
    return this._page;
  }

}
