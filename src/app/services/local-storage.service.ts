import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';


@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  private _user : string = "" 
  private _userPokemons : Pokemon[] = [];

  constructor() { 
    const user = localStorage.getItem('user');
    this._user = (user) ? user : "";
    this.getUserPockemonsFromLocalStorage();
  }

  // Gets the pokemons for a current user from local storage. 
  private getUserPockemonsFromLocalStorage() : void {
    if (this._user) {
      try {
        const userString = localStorage.getItem(this._user);
        if (userString) {
          this._userPokemons = JSON.parse(userString)
        } else {
          this._userPokemons = []
        }
      } catch(error) {
        console.error(error)
      }
    } else {
      this._userPokemons = []
    }
  }

  // Sets the current user.
  public setUser(user : string) : void {
    this._user = user;
    localStorage.setItem('user', user);
    this.getUserPockemonsFromLocalStorage();
  }

  // Returns the current user.
  public getUser() : string {
    return this._user;
  } 

  // Adds a pokemon to the current users pokemon.
  public addPokemon(pokemon : Pokemon) : void {
    if (!this._user) {
      return;
    }

    this._userPokemons.push(pokemon);
    localStorage.setItem(this._user, JSON.stringify(this._userPokemons))
  }

  // Removes the pokemon at index from the current users pokemon.
  public removePokemon(index : number) {
    if (!this._user) {
      return;
    }

    this._userPokemons.splice(index, 1);
    localStorage.setItem(this._user, JSON.stringify(this._userPokemons))
  }

  // Returns the current users pokemons.
  public getUserPokemons() : Pokemon[] {
    return this._userPokemons;
  }




}
