import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../services/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent {

  constructor(private readonly router: Router, private readonly localStorage : LocalStorageService) { }

  // Handles a user login to the page. 
  handleOnSubmit(user : string, valid: boolean | null) : void {
    if (!valid) {
      return;
    }
    this.localStorage.setUser(user)
    this.router.navigate(['/catalogue/1']);
  }
}
