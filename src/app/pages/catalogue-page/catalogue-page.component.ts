import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue-page.component.html',
  styleUrls: ['./catalogue-page.component.css']
})
export class CataloguePageComponent implements OnInit {
  public page : number = 0;

  constructor(private readonly api : ApiService, 
             private readonly localStorage : LocalStorageService, 
             private readonly route:ActivatedRoute,
             private readonly router: Router) { }


  ngOnInit(): void {
    this.page = parseInt(this.route.snapshot.params['page']);
    this.api.fetchPokemons(this.page);
  }

  // Fetches to the next pokemon catalog page.
  public handleNextPage() : void {
    this.page++;
    this.router.navigate([`/catalogue/${this.page}`]);
    window.scroll(0,0);
    this.api.fetchPokemons(this.page);
  }

  // Fetches to the previous pokemon catalog page.
  public handlePreviousPage() : void {
    this.page--;
    this.router.navigate([`/catalogue/${this.page}`]);
    window.scroll(0,0);
    this.api.fetchPokemons(this.page);
  } 

  // Adds a pokemon to the users pokemons.
  public handleAddPokemon(pokemon : Pokemon) : void {
    this.localStorage.addPokemon(pokemon)
  }


  // Gets the pages pokemons 
  get pokemons() : Pokemon[] {
    return this.api.pokemons();
  }

  // Gets if there is next page.
  get hasNext() : boolean {
    return this.api.hasNext();
  }

  // Gets if there is previous page.
  get hasPrevious() : boolean {
    return this.api.hasPrevious();
  }

  // Gets the id of all the users pikemons.
  get userPokemonIds() : string[] {
    return this.localStorage.getUserPokemons().map((pokemon : Pokemon) : string => pokemon.id);
  }
}
