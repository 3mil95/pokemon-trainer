import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css']
})
export class TrainerPageComponent implements OnInit {

  constructor(private readonly router: Router, private readonly localStorage : LocalStorageService) { }



  ngOnInit(): void {
  }

  public handleLogoutUser() {
    this.localStorage.setUser("");
    this.router.navigate(['/']);
  }

  public handleRemove(i : number) {
    this.localStorage.removePokemon(i)
  }

  // Gets the users pokemons
  get pokemons() {
    return this.localStorage.getUserPokemons()
  }
}
