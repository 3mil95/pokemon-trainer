import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(private readonly localStorage : LocalStorageService,  private readonly api : ApiService) { }
  
  // Gets the current catalog page loaded.
  get page() : number {
    return this.api.getPage();
  }

  // Gets the current user. 
  get user() : string {
    return this.localStorage.getUser();
  }
}
