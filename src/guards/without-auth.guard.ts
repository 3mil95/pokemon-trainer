import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WithoutAuthGuard implements CanActivate {

  constructor(private readonly router: Router) {
  }

  // Checks if there is a user logged in. if logged in redirect to login.
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!localStorage.getItem("user")) {
      return true
    }
    this.router.navigate([`/catalogue/1`]);
    return false;
  }
  
  
}
